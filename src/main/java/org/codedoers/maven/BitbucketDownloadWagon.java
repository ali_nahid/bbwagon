/*
 * Copyright 2018 Codedoers.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codedoers.maven;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.maven.wagon.AbstractWagon;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.codedoers.maven.bitbucket.BitbucketURLFactory;
import org.codedoers.maven.bitbucket.DownloadItem;
import org.codedoers.maven.bitbucket.Repository;
import org.codedoers.maven.bitbucket.Self;
import org.codedoers.maven.bitbucket.StaticCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitbucketDownloadWagon extends AbstractWagon {

	private static final Logger log = LoggerFactory.getLogger(BitbucketDownloadWagon.class);
	private static final StaticCache STATIC_CACHE = new StaticCache();

	private String url;
	private BitbucketCaller bitbucket;
	private BitbucketURLFactory urlFactory;
	private StaticCache.CacheElement currentCachedElement;

	@Override
	protected void openConnectionInternal() {
		if (getRepository().getUrl().equals(this.url)) {
			log.info("Already cached");
			return;
		}
		this.url = getRepository().getUrl();
		this.urlFactory = new BitbucketURLFactory(getRepository().getUrl());
		this.bitbucket = new BitbucketCaller(
				getAuthenticationInfo().getUserName(),
				getAuthenticationInfo().getPassword());
		if (!urlFactory.withRepository()) {
			currentCachedElement = STATIC_CACHE.getFromCacheOrFetch(this.url, this::fetchRepositories,
					this::fetchAllDownloadItems);
		}
	}

	@Override
	public void closeConnection() {
		log.info("Closing bitbucket repositories, nothing to do here...");
	}

	private Collection<Repository> fetchRepositories() {
		log.info("Fetching repositories from Bitbucket for - {}", this.url);
		return bitbucket.page(urlFactory.repositories(), Repository.FIELDS)
				.as(Repository.Page.class)
				.iterator(bitbucket)
				.stream()
				.collect(Collectors.toList());
	}

	@Override
	public void get(String filename, File file)
			throws TransferFailedException, ResourceDoesNotExistException {
		log.info("Fetching {} to file {}", filename, file.getPath());
		String name = normalizeFileName(filename);
		filterStreamAndDownload(streamDownloadItems(), name, file);
	}

	private Stream<DownloadItem> streamDownloadItems() {
		if (urlFactory.withRepository()) {
			return streamFromDefaultRepository();
		}
		return currentCachedElement.getDownloadItems().stream();
	}

	private List<DownloadItem> fetchAllDownloadItems(Collection<Repository> repositories) {
		return repositories.stream()
				.map(Repository::getDownloads)
				.map(Self::getHref)
				.map(href -> bitbucket.page(href, DownloadItem.FIELDS))
				.map(c -> c.as(DownloadItem.Page.class))
				.map(i -> i.iterator(bitbucket))
				.flatMap(PageableIterator::stream)
				.collect(Collectors.toList());
	}

	private Stream<DownloadItem> streamFromDefaultRepository() {
		return bitbucket.page(urlFactory.downloads(), DownloadItem.FIELDS).as(DownloadItem.Page.class).iterator(bitbucket).stream();
	}

	private void filterStreamAndDownload(Stream<DownloadItem> stream, String name, File file)
			throws TransferFailedException {
		Optional<String> found = findByName(stream, name);
		try {
			bitbucket.download(file, found.orElseThrow(() -> new ResourceDoesNotExistException(name + " not found")));
		} catch (Throwable ex) {
			log.error("Error download file " + name, ex);
			throw new TransferFailedException("Bitbucket error", ex);
		}
	}

	private Optional<String> findByName(Stream<DownloadItem> stream, String name) {
		Optional<String> found = stream
				.filter(di -> name.equals(di.getName()))
				.map(DownloadItem::getLinks)
				.map(DownloadItem.Links::getSelf)
				.map(Self::getHref)
				.findAny();
		return found;
	}

	@Override
	public boolean getIfNewer(String filename, File file, long l)
			throws TransferFailedException, ResourceDoesNotExistException {
		get(filename, file);
		return true;
	}

	@Override
	public void put(File file, String filename)
			throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
		log.info("Deploying {} as {}", file.getAbsolutePath(), filename);
		try {
			bitbucket.upload(file, urlFactory.upload(), normalizeFileName(filename));
		} catch (IOException ex) {
			throw new TransferFailedException("Bitbucket error", ex);
		}
	}

	private String normalizeFileName(String name) {
		return name.replace("/", "-");
	}

	@Override
	public boolean resourceExists(String resourceName) {
		String name = normalizeFileName(resourceName);
		return filterStreamAndCheckExists(streamDownloadItems(), name);
	}

	private boolean filterStreamAndCheckExists(Stream<DownloadItem> streamDownloadItems, String name) {
		return findByName(streamDownloadItems, name).isPresent();
	}

}
