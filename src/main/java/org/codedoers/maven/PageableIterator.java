/*
 * Copyright 2018 Codedoers.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codedoers.maven;

import java.util.Iterator;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.codedoers.maven.BitbucketCaller.CastableWrapper;
import org.codedoers.maven.bitbucket.Page;
import org.codehaus.plexus.util.StringUtils;

public class PageableIterator<T> implements Iterator<T> {

	private final BitbucketCaller bitbucket;
	private final Function<CastableWrapper, Page<T>> caster;
	private Page<T> page;
	private Iterator<T> iterator;

	public static <T> PageableIterator<T> create(BitbucketCaller bitbucket, Page<T> initial, Class<Page<T>> cl) {
		return new PageableIterator<>(bitbucket, initial, c -> c.as(cl));
	}

	public PageableIterator(BitbucketCaller bitbucket, Page<T> initial, Function<BitbucketCaller.CastableWrapper, Page<T>> caster) {
		this.bitbucket = bitbucket;
		this.caster = caster;
		this.page = initial;
		this.iterator = page.getValues().iterator();
	}

	@Override
	public boolean hasNext() {
		if (iterator.hasNext()) {
			return true;
		}
		if (StringUtils.isNotBlank(page.getNext())) {
			page = caster.apply(bitbucket.nextPage(page.getNext()));
			iterator = page.getValues().iterator();
			return hasNext();
		}
		return false;
	}

	@Override
	public T next() {
		return iterator.next();
	}

	public Stream<T> stream() {
		Iterable<T> iterable = () -> this;
		return StreamSupport.stream(iterable.spliterator(), false);
	}

}
