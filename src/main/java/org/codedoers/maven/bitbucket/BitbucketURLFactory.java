/*
 * Copyright 2018 Codedoers.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codedoers.maven.bitbucket;

import java.util.regex.Matcher;
import org.codedoers.maven.BitbucketAPI;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitbucketURLFactory {

	private static final Logger log = LoggerFactory.getLogger(BitbucketURLFactory.class);

	private final String team;
	private final String repository;

	public BitbucketURLFactory(String mvnRepositoryURL) {
		Matcher matcher = BitbucketAPI.EXTRACTION_PATTERN.matcher(mvnRepositoryURL);
		assertTeamFound(matcher);
		this.team = matcher.group(1);
		this.repository = matcher.group(2);
	}

	public String repositories() {
		return BitbucketAPI.REPOSITORIES + team;
	}
	
	public String downloads() {
		return downloads(repository);
	}
	
	public String downloads(String repositorySLUG) {
		return BitbucketAPI.REPOSITORIES + team + "/" + repositorySLUG + "/downloads/";
	}

	public String download(String fileName) {
		return download(repository);
	}

	public String download(String repositorySLUG, String fileName) {
		return downloads(repositorySLUG) + fileName;
	}
	
	public String upload() {
		return upload(repository);
	}
	
	
	private String upload(String repositorySLUG) {
		return BitbucketAPI.REPOSITORIES + team + "/" + repositorySLUG + "/downloads";
	}

	private void assertTeamFound(Matcher matcher) {
		if (!matcher.find()) {
			log.error("Match for team failed from");
			throw new RuntimeException("Match for team failed from");
		}
	}

	public boolean withRepository() {
		return StringUtils.isNotBlank(repository);
	}


}
