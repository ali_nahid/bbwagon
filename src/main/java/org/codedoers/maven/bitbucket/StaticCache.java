package org.codedoers.maven.bitbucket;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class StaticCache {

	private Map<String, CacheElement> cache = new HashMap<>();

	public synchronized CacheElement getFromCacheOrFetch(String url,
			Supplier<Collection<Repository>> fetchRepositories,
			Function<Collection<Repository>, List<DownloadItem>> fetchAllDownloadItems) {
		return cache.computeIfAbsent(url, createFetcher(fetchRepositories, fetchAllDownloadItems));
	}

	private Function<String, CacheElement> createFetcher(Supplier<Collection<Repository>> fetchRepositories,
			Function<Collection<Repository>, List<DownloadItem>> fetchAllDownloadItems) {
		return url -> {
			Collection<Repository> repos = fetchRepositories.get();
			List<DownloadItem> items = fetchAllDownloadItems.apply(repos);
			return new CacheElement(repos, items);
		};
	}


	public class CacheElement {
		private final Collection<Repository> repositories;
		private final List<DownloadItem> downloadItems;

		private CacheElement(Collection<Repository> repositories,
				List<DownloadItem> downloadItems) {
			this.repositories = repositories;
			this.downloadItems = downloadItems;
		}

		public Collection<Repository> getRepositories() {
			return repositories;
		}

		public List<DownloadItem> getDownloadItems() {
			return downloadItems;
		}
	}
}
