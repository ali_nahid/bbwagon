/*
 * Copyright 2018 Codedoers.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codedoers.maven.bitbucket;

public class DownloadItem {

	public static final String FIELDS = "pagelen,size,next,page,values.name,values.links.self";

	private String name;
	private Links links;

	public String getName() {
		return name;
	}

	public Links getLinks() {
		return links;
	}

	public static class Page extends org.codedoers.maven.bitbucket.Page<DownloadItem> {

	}

	public static class Links {

		Self self;

		public Self getSelf() {
			return self;
		}

	}

}
